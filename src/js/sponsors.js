app.sponsors = {
  platinum: [],
  gold: [
    {
      name: 'Learning A-Z',
      url: 'https://www.learninga-z.com/',
      logo: 'assets/images/sponsors/learninga-z.svg',
    },
    {
      name: 'Twilio',
      url: 'https://www.twilio.com/',
      logo: 'assets/images/sponsors/twilio.svg',
    },
  ],
  silver: [
    {
      name: 'Yochana IT Solutions',
      url: 'http://yochanait.com',
      logo: 'assets/images/sponsors/yochana.png',
    },
    {
      name: 'KNP University',
      url: 'https://knpuniversity.com/',
      logo: 'assets/images/sponsors/knpu-logo.svg',
    },
    {
      name: 'Open Source Mental Illness',
      url: 'https://osmihelp.org',
      logo: 'assets/images/sponsors/osmi_logo.svg',
    },
    {
      name: 'Barracuda Networks',
      url: 'https://barracuda.com',
      logo: 'assets/images/sponsors/barracuda.svg',
    },
    {
      name: 'Thermo',
      url: 'https://thermo.io',
      logo: 'assets/images/sponsors/thermo_logo.svg',
    },
    {
      name: 'Learning Experiences',
      url: 'https://www.learnxp.com',
      logo: 'assets/images/sponsors/learning-xp.svg',
    },
    {
      name: 'MySQL',
      url: 'https://www.mysql.com',
      logo: 'assets/images/sponsors/mysql.svg',
    },
  ],
  bronze: [],
  community: [
    {
      name: 'SunshinePHP',
      url: 'http://sunshinephp.com',
      logo: 'assets/images/sponsors/sunshinephp.png',
    },
  ],
  inkind: [
    {
      name: 'JetBrains',
      url: 'https://www.jetbrains.com',
      logo: 'assets/images/sponsors/jetbrains.svg',
    },
  ],
};
